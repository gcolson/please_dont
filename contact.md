---
layout: page
title: titles.contact
namespace: contact
permalink: /contact/
---

# {% t pages.contact.main_title %}

<form action="https://airform.io/contact@pleasedont.fr" method="POST">
  <input type="text" id="name" name="name" placeholder="{% t pages.contact.name %}:" autocomplete="off">
  <input type="text" id="email" name="email" placeholder="{% t pages.contact.email %}:" autocomplete="off">
  <textarea rows="5" id="message" name="message" placeholder="{% t pages.contact.message %}:" autocomplete="off"></textarea>
  <input type="submit" value="[ submit ]">
</form>

Vous pouvez aussi m'écrire directement à l'adresse pleasedontmakemusic [at] gmail.com