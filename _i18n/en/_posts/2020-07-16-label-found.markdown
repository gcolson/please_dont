---
layout: post
title:  hurray y found a record label
date:   2020-07-16 09:05:04 +0200
categories: news
---
I finally managed to find some record labels that are crazy enough to accept to release my album ! When I say crazy enough, I mean that I don't plan to play this music live, which means no concert to promote and sell the record, and not much money for me nor the record labels. My idea was to mke my music listenable for whoever wants to listen to it. Nothing more.

<br>

Why that choice ? Because trying to play this music live takes a lot of energy : booking some gigs, selling its project, rehearse some live formula of the songs and then organize and do the tour. This whole work implies to get other people working on the project, without being able to pay them as much as they should be, and I try to avoid that because this is a very personnal project.

<br>

I also have other bands ([Rouuge](https://www.instagram.com/rou.uge) and [Wild Wild Waves](http://wildwildwaves.fr/)), with which I often play live so I guess I don't really have the need to play live with my solo project. By the way, go listen to these other projects, you might like it !

<br>
<br>

Anyway, these 2 labels that are willing to release my album are [L'Affect Records](https://laffectrecords.bandcamp.com/) and [AB Records](https://abrecords.bandcamp.com/). These are some really cool friends who release some beautiful music. I must say I'm very happy knowing that my record will be released by them and that I become part of these two nice families.

<br>

Save the date : ["Wandering Around"]({{ '/en/musique' | relative_url }}) should be released at the beginning of September, after more than 2 years working on it. Can't wait !