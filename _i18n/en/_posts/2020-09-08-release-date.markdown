---
layout: post
title:  my first album is to be released on 9/11
date:   2020-09-08 06:12:04 +0200
categories: news
---
So, it seems that "wandering_around" will be released on the 11th of September. It will be first released exclusively on the [AB Records bandcamp](https://abrecords.bandcamp.com/). Then you'll find it on the [l'Affect Records bandcamp](laffectrecords.bandcamp.com) and finally on all streaming platforms the 25th of Septembre, so that everyone will be happy (including me)

<br>

I must say i can't wait for you to discover that LP, after 3 years of composing, recording, re-recording (for better sound), mixing at [Sample & Hold Studio](https://sampleandhold.fr/), mastering and preparing the release. That's quite a huge project that i've comlpleted and i'm kind of proud of it. I just hope now that you'll like it as much as i enjoyed making it.

<br>

This album means a lot to me : it represents a part of my life, a time of changes that have made me grow up, for sure. I wrote theses songs all along this reconstruction time, whithout a real goal in mind. But everything was very natural and i finally decided to make an album out of it (i have to say i kind of like the LP format). These are still songs that you can listen to individually, but you should definitely listen to it as a whole to get everything i intended to share.

<br>
<br>

I won't say much more because i think you should just listen to it to understand everything. You can find the lyrics on the ["listen to his music"]({{ '/en/musique' | relative_url }}) part of my website (at the bottom of the page, just click on the song titles). It can help understand what i'm saying.

<br>

I hope you enjoy it. Don't hesitate to [tell me what you think of it]({{ '/en/contact' | relative_url }}) if you feel like it : it's always nice to hear it !