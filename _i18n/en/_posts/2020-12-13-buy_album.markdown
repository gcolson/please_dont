---
layout: post
title:  you can now buy my album directly from my website
date:   2020-12-13 12:33:04
categories: news
---

today is 13/12, or ACAB day as some people like to call it.

anyway, it's sunday before all, and that give me time to add to this website a brand new ["shop" page](/en/shop). Starting today, this page allows you to buy a limited Compact Disc edition of my album. ["wandering_around"](/en/music).

<br> 

i've got only 50 copies to sell, so hurry up, there won't be one for everyone !

that could be a lie though, counting the views of my [last music video](https://www.youtube.com/watch?v=CxzXF5R6H_Y), and estimating the number of people that still use CDs...

<br>

whatever, i don't care : i mostly wanted to make a CD version of the album for me (yes, i have and listen to a lot of CDs), for my close friends and family and also for posterity !

it's only 10€, so [go for it](/en/shop) !