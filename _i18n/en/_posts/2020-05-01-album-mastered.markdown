---
layout: post
title:  my debut album is mastered !
date:   2020-05-01 12:15:04 +0200
categories: news
---

After a few week of working hard, Allan finally made it to finish the mastering of my debut album "wandering_around". It is now all ready to be released on the internet and everywhere else.

I'm now looking for labels to help me release it correctly. If you have any idea [do not hesitate]({% link contact.md %}).

<br>


{% highlight ruby %}
def master_album(name)
  if Album.find(name).get_master
    puts "Hi, the album #{name} is finally mastered. Well done !"
  end
end
master_album('wandering_around')
#=> Hi, the album wandering_around is finally mastered. Well done !
{% endhighlight %}
