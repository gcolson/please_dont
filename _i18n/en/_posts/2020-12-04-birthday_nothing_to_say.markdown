---
layout: post
title:  a new music video for my birthday
date:   2020-12-04 12:00:01
categories: news
---

Today is the 4th of December and it is my birthday ! To celebrate i decided to publish another music video of one song of my album. If i say "another", it's because maybe you noticed that a picture of [lisbeth](https://www.youtube.com/watch?v=gDBk3OpvHHU) appeared on this website's homepage in the last weeks. On this picture there is a link to the music video of the last song of my debut album : ["lisbeth"](/lyrics/lisbeth).

<br>

Anyway, "nothing_to_say" is, as its name stands for, a song that does not tell something special. Nothing else that the fact that i had nothing special to say the day i wrote the lyrics. What's better to illustrate that but some contemplative pictures taken through my binoculars during some of my wanders in the french contryside ?
I let you discover that  : https://www.youtube.com/watch?v=CxzXF5R6H_Y

<br>

Fun fact (not so fun but anyway) : it's one of the song that took me the less time to write. I think i composed it and wrote the lyrics in only Pour l'anecdote c'est un des morceaux que j'ai écrit et composé le plus vite : ça a dû être bouclé en une après-midi  afernoon ! First i had the musical idea : bass line and drums, then the melody, on which i didn't know what lyrics to sing.. So i wrote these lyrics talking about that and recorded the demo straight after. All that in only 3 or 4 hours !

<br><br>

Still a big thanks to [AB Records](https://abrecords.bandcamp.com/), [L'Affect Records](https://laffectrecords.bandcamp.com/), Théo Des Neiges and his [Sample & Hold Studio](https://sampleandhold.fr/) for recording and mixing, Nalla Aïssoug for mastering, Ugo Del Rosso for the precious editing help, [Archipel](http://www.archipelmusic.com/), and everyone who supported me.

