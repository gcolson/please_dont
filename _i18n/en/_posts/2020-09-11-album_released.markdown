---
layout: post
title:  my album was just released !
date:   2020-09-11 11:23:31
categories: news
---

So, that's it then, my first album is released today. Kind of a crazy project and i'm proud to see that coming out to the world !

<br>

This album represents a part of my life, a time of changes that have made me grow up for sure. I wrote these songs during the year 2018, during some journeys, hikes or mental trips, whithout knowing i was gonna make an album out of it. But everything was very natural and not so surprising when you know how much i love albums.

<br>

These are 10 songs that you can listen to separatly, but i must recommend to take 40 chilling minutes to listen to it all the way through.
Anyway, i just hope you will have as much joy listening to it as i had recording it !

<br>

I'm not gonna say anymore bullshit. You should just click on the following link and simply listen to that -> https://abrecords.bandcamp.com/album/wandering-around-2. 


I also put the lyrics on the [music page of this website](/en/music/) (bottom of the page, just click on the songs titles) : it can help understand what i'm saying with my nice french accent.
Have a nice listening !

<br><br>

I must add 1000 thanks to [AB Records](https://abrecords.bandcamp.com/), [L'Affect Records](https://laffectrecords.bandcamp.com/), Théo Des Neiges et son [Sample & Hold Studio](https://sampleandhold.fr/), Nalla Aïssoug, Ugo Del Rosso, [Archipel](http://www.archipelmusic.com/), and the people that have support me one way or another during these 3 years.

