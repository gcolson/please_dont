wandering_around
================

<br>

<iframe style="border: 0; width: 100%; min-height: 600px;" src="https://bandcamp.com/EmbeddedPlayer/album=2792607754/size=large/bgcol=333333/linkcol=ffffff/minimal=true/transparent=true/" seamless><a href="https://laffectrecords.bandcamp.com/album/ar24-wandering-around">[AR24] wandering_around by please_don&#39;t</a></iframe>

<br>

[please_don't](https://soundcloud.com/pleasedontmusic),is nothing more than just another weird guy who, after 10 years of playing music with other bands ([Wild Wild Waves](http://www.wildwildwaves.fr), [Raoul Vignal](http://www.talitres.com/fr/artistes/raoul-vignal.html), [Rouuge](https://www.facebook.com/Rouugemusic), VicDub Experience etc) started writing his own songs and decided to record them with his synthesizers and his drums. 

<sub><sup>Geek details : I play on a juno 60, a wurlitzer 200A and a SH-101. The drum is a Tama Rockstar lol.</sup></sub>

That weird guy had sentences and melodies floating about in his head: songs talking about the things he goes through, the things he observes, in him and in others. The songs craft some kind of chill pop, tinged with harmonic flourishes, heady melodies and uncanny yet catchy bass patterns.

There are songs about walking in nature or in the city, about the years going by, about Lisbeth (his cat) and also about what’s happening in the weird guy’s head - but that’s not always easy to explain.

<br>

Please_don’t is this guy who’s been listening a lot to Timber Timbre, Mac Demarco, Grizzly Bear and Lowly, this guy who found pleasure in creating all this and decided to record it a bit better than with his two mics. To be able to share it with others. So he went to see his friend Théo at the Sample and Hold Recording studio in Lyon, and they recorded it - dirty and tasty, just as they like it.

The following summer, that guy went hiking in Brittany and a friend of his joined him on the beach and took some pics with his analog camera. The pics were nice so he decided they would be the cover pictures of the album.
Then, on that other day, the weird guy took videos of his cat Lisbeth and he called that other friend of his who made a music video out of it, for the song about his cat. Pretty handy.

<br>

Finally, 2 years after writing the first songs, the album is good and ready. It’s called wandering_around.

That weirdo is no one else but me, and I’m writing this text to explain what I wanted for my songs to whoever feels like reading it. But what I'd really like now, is that those who are interested in this music easily manage to find it, to listen to it and to have just as much pleasure as I had while recording it. 

<br>

And to be honest, that weird guy - or should I say “I” – is eager to go back to his gardening or record other songs… 

<br>

![please_don't](/assets/images/back.jpg){:class=""}


Tracklist
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ '/lyrics/please_don't' | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})

<br>

All of this will be available as soon as I find a record label which is interested to release it as it should.

Before that, I could be able to give you a private link if you [ask kindly]({% link contact.md %}). He who tries nothing has nothing.