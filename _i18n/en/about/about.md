please_dont is a lonely gardener. Instead of growing some lettuce, he grows songs in his loneliness compost. As lettuces, some are sweet, other are bitter, but they're always round, round, round like the earth.

<br>

![please_don't](/assets/images/please_don't.jpg){:class=""}

<br>

In its debut album wandering_around, he walks with his nonchalance. 10 songs that wander between the city and the countryside, like a waking dream in a steam train. Peaceful. This railway go right through us as if we were the landscape, from sunshine to rain.

<br>

His instruments drag their feet like awaking from a deep sleep way too soon; Synts are lazy and let us know it. They wake up long enough for an epic take-off, a jazz bubble and suddenly go down in the analogic fog.

<br>

We could easily tell ourselves that this disillusioned gardener is making fun of us; We would be wrong. On the contrary, he offers us to take on his wide open gaze, to catch up a squirrel's attention, a butterfly's fly or the cricket's singing with him. This world is kind of magical for people who know were to look; please_don't offer you its magnifying glass.