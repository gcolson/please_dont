wandering_around
================

<br>

<iframe style="border: 0; width: 100%; min-height: 600px;" src="https://bandcamp.com/EmbeddedPlayer/album=2792607754/size=large/bgcol=333333/linkcol=ffffff/minimal=true/transparent=true/" seamless><a href="https://laffectrecords.bandcamp.com/album/ar24-wandering-around">[AR24] wandering_around by please_don&#39;t</a></iframe>

<br>

[please_don't](https://soundcloud.com/pleasedontmusic), c'est rien d'autre qu'un gars bizarre de plus qui, après 10 ans à jouer de la musique avec d'autres ([Wild Wild Waves](http://www.wildwildwaves.fr), [Raoul Vignal](http://www.talitres.com/fr/artistes/raoul-vignal.html), [Rouuge](https://www.facebook.com/Rouugemusic), VicDub Experience etc) se met un jour à écrire des chansons et décide de les enregistrer avec ses synthés et sa batterie. 

Et puis il se trouve qu'il a quelques phrases et mélodies qui lui trottent dans la tête : des chansons qui parlent de ce qu'il vit, ce qu'il observe, chez lui et chez les autres. Ça donne une sorte de chill pop, pleine d'envolées harmoniques, de mélodies entêtantes et de lignes de basse un peu bizarre mais quand même efficaces. 

Ça parle de marcher dans la nature ou dans la ville, des années qui passent, de Lisbeth (son chat) et aussi de ce qui se passe dans sa tête et c'est pas toujours facile à expliquer.

<br>

please_don't, c'est donc avant tout ce gars, qui a beaucoup écouté Timber Timbre, Mac Demarco, Grizzly Bear et Lowly, qui prend du plaisir à créer tout ça et qui décide de l'enregistrer un peu mieux qu'avec ses 2 micros. Histoire de pouvoir le partager avec d'autres. 

Alors il va voir son copain Théo au Sample and Hold Recording Studio à Lyon et ils enregistrent ça tout bien ils aiment. 
Puis l'été suivant il va randonner en Bretagne et un beau jour un autre copain le rejoint sur la plage et prend des photos avec son argrentique. Les photos sont jolies donc il décide d'en faire la pochette de son album. Et puis un autre jour le gars bizarre prend des vidéos de Lisbeth, il appelle l'autre copain qui, ni une ni deux, en fait un clip pour le morceau qui parle de son chat. Pas bête.

<br>

Finalement, 2 ans après avoir écris les premières chansons, l'album est prêt, tout beau tout fini. Il s'appelle wandering_around. 

Et le gars bizarre c'est moi et je me retrouve à écrire ça, pour essayer d'expliquer à qui voudra bien le lire ce que j'ai voulu faire avec ces chansons. Mais ce que j'aimerais maintenant, c'est juste que celles et ceux que ça intéresse les trouvent, les écoutent et prennent du plaisir à faire ça comme j'ai pris du plaisir à les enregistrer. 

<br>

Parce que moi, le gars bizarre, pour tout vous dire, j'ai surtout envie de retourner jardiner ou enregistrer d'autres chansons... 

<br>

![please_don't](/assets/images/back.jpg){:class=""}


Tracklist
---------
1 - [walking_in_the_city]({{ site.baseurl }}/lyrics/walking_in_the_city)
2 - [nothing_to_say](/lyrics/nothing_to_say)
3 - [in_and_out](/lyrics/in_and_out)
4 - [wandering_around](/lyrics/wandering_around)
5 - [put_it_out](/lyrics/put_it_out)
6 - [please_don't](/lyrics/please_don't)
7 - [round_down_round](/lyrics/round_down_round)
8 - [441](/lyrics/441)
9 - [through_the_years](/lyrics/through_the_years)
10 - [lisbeth](/lyrics/lisbeth)

<br>

Tout ça sera évidemment disponible et écoutable très vite (peut-être bien le 11 Septembre) grâce aux gentils labels [L'Affect Records](https://laffect.fr/) et [AB Records](https://abrecords.bandcamp.com/). 

En attendant, peut-être que si c'est [demandé gentiment]({% link contact.md %}) je peux vous envoyer un lien privé huhuhu ? Qui ne tente rien n'a rien.