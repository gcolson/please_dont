---
layout: post
title:  mon album vient de sortir
date:   2020-09-11 11:23:31
categories: news
---

Alors ça y est, c'est aujourd'hui que sort ce fameux premier album. Un projet assez foufou, que je suis vraiment content d'avoir mené à terme. 

<br>

Cet album, il représente un petit bout de ma vie, une période de bouleversements assez généralisés qui m'ont fait grandir, pour sur. J'ai écris ces chansons aux alentours de l'année 2018, au cours de nombreux voyages, ballades et autres pérégrinations mentales, sans vraiment savoir que j'allais en faire un album. Mais tout s'est fait très naturellement et c'est pas si étonnant finalement quand on connaît mon faible pour le format album.

<br>

Ce sont 10 chansons qu'on peut écouter séparément, mais l'idéal reste à mon sens de se réserver 40 bonnes minutes de chill pour écouter ça d'un bout à l'autre. 
Quoi qu'il en soit j'espère que vous prendrez autant de plaisir à l'écouter que j'ai pris du plaisir à le faire !

<br>

Je vais pas en dire beaucoup plus, le plus simple étant d'écouter tout ça ici -> https://abrecords.bandcamp.com/album/wandering-around-2. 


J'ai aussi mis les paroles sur [la page musique](https://www.pleasedont.fr/music/) (tout en bas, faut cliquer sur les titres des chansons) : ça peut aider à comprendre ce que je raconte.
Allez, bonne écoute !

<br><br>

Avec mille merci à [AB Records](https://abrecords.bandcamp.com/), [L'Affect Records](https://laffectrecords.bandcamp.com/), Théo Des Neiges et son [Sample & Hold Studio](https://sampleandhold.fr/), Nalla Aïssoug, Ugo Del Rosso, [Archipel](http://www.archipelmusic.com/), mais aussi toutes les personnes qui m'ont soutenu d'une manière ou d'une autre durant ces 3 ans.

