---
layout: post
title:  l'album sort le 11 Septembre
date:   2020-09-08 06:12:04 +0200
categories: news
---
Ça sera donc le 11 Septembre 2020 que sortira "wandering_around". La sortie sera dans un premier temps en exclusivité sur [le bandcamp de AB Records](https://abrecords.bandcamp.com/). On étendra ça ensuite au [bandcamp de l'Affect Records](laffectrecords.bandcamp.com) puis à touts les plateformes de streaming le 25 Septembre, pour votre plus grand bonheur (et aussi le mien)

<br>

Très franchement j'ai hâte de vous faire découvrir tout ça, après 3 années à composer, enregistrer, re-enregistrer mieux et mixer au [Sample & Hold Studio](https://sampleandhold.fr/), masteriser et puis préparer la sortie. Un gros projet que je suis assez fier d'avoir mené au bout et pour tout vous dire je suis très content du résultat ! J'espère maintenant que vous prendrez autant de plaisir à l'écouter que j'ai pris du plaisir à le faire.

<br>

Cet album, il représente un bout de ma vie, une période de boulversements assez généralisés qui m'ont fait grandir, pour sur. J'ai écris ces chansons tout au long de cette période de reconstruction, sans vraiment savoir que j'allais en faire un album. Mais tout s'est fait très naturellement et il faut dire que j'ai un faible pour ce format album. Ça reste des chansons, qu'on peut écouter séparément, mais l'idéal reste de l'écouter d'un bout à l'autre à mon avis.

<br>
<br>

Je vais pas en dire beaucoup plus, le plus simple pour comprendre tout ça étant de l'écouter. J'ai aussi mis les paroles sur la partie ["écouter sa musique"]({{ '/musique' | relative_url }}) de mon site (tout en bas, faut cliquer sur les titres des chansons), ça peut aider à comprendre ce que je raconte.

<br>

Allez, bonne écoute et n'hésitez pas à me dire [ce que vous en pensez]({{ '/contact' | relative_url }}) si vous en avez l'envie, ça fait toujours plaisir !