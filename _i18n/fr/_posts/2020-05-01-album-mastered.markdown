---
layout: post
title:  mon premier album est masterisé !
date:   2020-05-01 12:15:04 +0200
categories: news
---
Après quelques semaines de travail, Allan a finalement fini de masteriser mon premier album "wandering_around". Il est donc tout prêt pour sortir sur les internet et partout ailleurs ! 

Je cherche maintenant des labels pour m'aider à sortir ça bien comme il faut, si vous avez des idées [n'hésitez pas]({% link contact.md %}) hein.

<br>


{% highlight ruby %}
def master_album(name)
  if Album.find(name).get_master
    puts "Hi, the album #{name} is finally mastered. Well done !"
  end
end
master_album('wandering_around')
#=> Hi, the album wandering_around is finally mastered. Well done !
{% endhighlight %}
