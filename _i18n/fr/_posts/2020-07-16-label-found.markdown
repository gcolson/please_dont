---
layout: post
title:  youpi j'ai trouvé un label.
date:   2020-07-16 09:05:04 +0200
categories: news
---
J'ai finalement réussi à trouver des labels assez dingos pour bien vouloir sortir mon album ! Je dis assez fou parce que, clairement, je n'ai pas prévu de monter de formule live ni de faire de concerts pour promouvoir le disque (et donc faire de l'argent). L'idée étant simplement de rendre ma musique disponible à qui voudra bien l'écouter, sans vraiment plus de prétention. 

<br>

Pourquoi ce choix ? Parce que chercher à jouer sa musique en live est un travail qui demande beaucoup d'énergie : démarcher des salles, vendre son projet, répéter une formule live, puis organiser et faire la tournée. Tout un travail qui nécessite d'impliquer d'autres personnes sur le projet sans être sûr de pouvoir leur assurer une rémunération correcte, ce que je préfère éviter pour le moment sachant que le propos artistique est quand même très personnel.

<br>

D'autant que j'ai déjà d'autres projets plus collectifs avec lesquels je joue en live : [Rouuge](https://www.instagram.com/rou.uge) et [Wild Wild Waves](http://wildwildwaves.fr/)). Allez y jeter une oreille à l'occasion !

<br>
<br>

Bref, ces deux labels qui veulent bien sortir mon album, c'est [L'Affect Records](https://laffectrecords.bandcamp.com/) et [AB Records](https://abrecords.bandcamp.com/), des copain⋅es super cool et qui sortent de la très jolie musique. Autant dire que je suis plus qu'heureux de rejoindre et faire collaborer ces deux belles familles !

<br>

L'album devrait donc sortir début Septembre, après 2 ans et demi de gestation. J'ai hâte !