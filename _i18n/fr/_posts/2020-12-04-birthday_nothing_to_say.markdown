---
layout: post
title:  un nouveau clip pour mon annif !
date:   2020-12-04 12:00:01
categories: news
---

Aujourd'hui, le 4 Décembre, c'est mon anniversaire ! Pour l'occasion j'ai décidé de sortir un 2e clip extrait de mon album. Le dis 2e parce que, je sais pas si vous avez vu mais il y a quelques semaines une image de [lisbeth](https://www.youtube.com/watch?v=gDBk3OpvHHU) est apparu sur la page d'accueil, avec dessus un lien vers le clip de la dernière chanson de mon album [lisbeth](/lyrics/lisbeth).

<br>

"nothing_to_say", comme son nom l'indique, c'est une chanson qui ne raconte pas grand chose. En tous cas pas grand chose d'autre que le fait que je n'avais rien de spécial à dire quand j'ai écris les paroles. Et pour illustrer ça, quoi de mieux que des images contemplatives prises à travers mes jumelles lors de mes vagabondages à droite à gauche (wandering around en anglais, qui a donné le nom de l'album) ?
Je vous laisse découvrir ça  : https://www.youtube.com/watch?v=CxzXF5R6H_Y

<br>

Pour l'anecdote c'est un des morceaux que j'ai écrit et composé le plus vite : ça a dû être bouclé en une après-midi ! J'ai d'abord eu l'idée musicale, la ligne de basse et la rythmique, puis la ligne mélodique, sur laquelle je ne savais pas vraiment quoi raconter, donc j'ai écris ces paroles qui expliquent cela, et enregistré la démo dans la foulée. Tout ça en 3-4h !

<br><br>

Avec toujours un grand merci à [AB Records](https://abrecords.bandcamp.com/), [L'Affect Records](https://laffectrecords.bandcamp.com/), Théo Des Neiges et son [Sample & Hold Studio](https://sampleandhold.fr/) au mixage et prise de son, Nalla Aïssoug au mastering, Ugo Del Rosso pour l'aide précieuse au montage, [Archipel](http://www.archipelmusic.com/), mais aussi toutes les personnes qui m'ont soutenu.

