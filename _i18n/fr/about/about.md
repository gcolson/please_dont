please_dont est un jardinier solitaire. À la place de salades, il fait pousser des chansons dans le terreau de sa solitude. Comme les salades elles peuvent être douces ou amères suivant la variété, mais toujours rondes, rondes, rondes comme le monde. 

<br>

![please_don't](/assets/images/please_don't.jpg){:class=""}

<br>

Il promène sa nonchalance dans wandering_around, son premier LP. Dix chansons qui se tissent d'aller-retours entre ville et campagne comme un rêve éveillé dans un train à vapeur, tranquille. Un rail qui nous traverse comme si on était le paysage, passant par le soleil et la pluie.

<br>

Ses instruments trainent les pieds comme sortis du sommeil trop tôt; les synthés ont la flemme et ils nous le font savoir. Ils se réveillent le temps d'une énvolée épique, d'une bulle de jazz et replongent aussitôt dans le brouillard analogique.

<br>

On pourrait être tenté de se dire que ce jardinier désabusé se moque de nous depuis son potager; il n'en est rien. Il nous propose au contraire d'adopter son regard écarquillé, de capter avec lui l'attention d'un écureuil, le vol d'un papillon, le chant du criquet. Ce monde est magique pour qui sait regarder; please_dont te prête sa loupe.