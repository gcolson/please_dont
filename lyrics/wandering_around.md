---
title: titles.wandering_around
layout: page
permalink: /lyrics/wandering_around
---

wandering_around
=====
<br>
instrumental


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
**4 - wandering_around**
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})
