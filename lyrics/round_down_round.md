---
title: titles.round_down_round
layout: page
permalink: /lyrics/round_down_round
---

round_down_round
================

<br>

my mind is going round, down, round
my body is just sitting here
everyone's going round, round, in mound
and i'd like to just disappear

<br>

wish i could switch from round to up
just press a button and stop
all this mess i'm seeing
everywhere i'm watching
but i'm just sitting here

<br>

my mind, which's going round, down, round
tells my body to come with her
tells me just disregard the crown (look at the earth)

<br>

so i'm trying to stand up
concentrate on this buttercup
but i can't stop seeing
everyone just squashing it

<br>

i'm just going to sit here
i'm going to sit right here
(i'm going to sit right here)


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
**7 - round_down_round**
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})
