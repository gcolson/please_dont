---
title: titles.nothing_to_say
layout: page
permalink: /lyrics/nothing_to_say
---

nothing_to_say
==============
<br>
i can't help but play
oh, can't help but play
but i got nothing to say
oh, nothing to say
<br>
so, you'd like to hear
what i got to say
for now, i got nothing
oh, nothing to say
<br>
i can't help but play
oh, can't help but play
but i got nothing to say
oh, nothing to say
<br>
why should i say something
i mean, i'm so boring, anyway
for now, we'll keep playing
whatever works, darling
<sub><sup>just to say</sup></sub>
<br>
i can't help but play
oh, can't help but play
but i got nothing to say
oh, nothing to say


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
**2 - nothing_to_say**
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})