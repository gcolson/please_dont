---
title: titles.please_don't
layout: page
permalink: /lyrics/please_don't
---

please_don't
============

<br>

you're feeling strange again 
<sub><sup>please don't</sup></sub>
you should let out the pain 
<sub><sup>you won't</sup></sub>
you're messing with your brain 
<sub><sup>please don't</sup></sub>
you gotta stop the rain 
<sub><sup>you won't</sup></sub>
<br>
seems easy to understand
how life flows
when everything go just fine
trouble comes knocking at the door

<br>

it should be pretty simple
as long as you know yourself well
to go through it without trouble
if you could just listen to your brain
<br>
you're feeling strange again 
<sub><sup>please don't</sup></sub>
you should let out the pain 
<sub><sup>you won't</sup></sub>
you're messing with your brain 
<sub><sup>please don't</sup></sub>
you gotta stop the rain 
<sub><sup>you won't</sup></sub>

<br>

you're feeling strange again 
<sub><sup>please don't</sup></sub>
you should let out the pain 
<sub><sup>you won't</sup></sub>
you're messing with your brain 
<sub><sup>please don't</sup></sub>
you gotta stop the rain 
<sub><sup>you won't</sup></sub>

<br>

you're getting far from sane 
<sub><sup>please don't</sup></sub>
you need to understand 
<sub><sup>you won't</sup></sub>
just following your friends 
<sub><sup>please don't</sup></sub>
while you should step back while you can 
<sub><sup>you won't</sup></sub>

<br>

you're feeling strange again 
<sub><sup>please don't</sup></sub>
you should let out the pain 
<sub><sup>you won't</sup></sub>
you're messing with your brain 
<sub><sup>please don't</sup></sub>
you gotta stop the rain 
<sub><sup>you won't</sup></sub>


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
**6 - please_don't**
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})