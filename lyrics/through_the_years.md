---
title: titles.through_the_years
layout: page
permalink: /lyrics/through_the_years
---

through_the_years
=================

<br>
time's passing
i'm going through the years
keep, keep thinking 
about all the lives i've shared

<br>

few people where there
on my train departure
but most, most of them
are still waiting in the future
<br>
i wanted to ask you some things about your past
i wanted to tell you i do care about that
i just wish we could go back
whatch our movie from the start
<br>
we all are
the sum of our experiences
but we hide it far, far, far, far
away behind some fences

<br>

the more we grow up
the thicker it grows
and i'd like to blow it up
but it's harder than i first supposed
<br>
i wanted to ask you some things about your past
i wanted to tell you i do care about that
i just wish we could go back
whatch our movie from the start


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
**9 - through_the_years**
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})
