---
title: titles.put_it_out
layout: page
permalink: /lyrics/put_it_out
---

put_it_out
==========

<br>

waking from a dream
trying to feel it a little more
knowing exactly what you just lived
but so hard to put it in words

<br>

so what are you gonna do ?
what are you gonnas say ?
<sub><sup>keep it for yourself</sup></sub>
what are you gonna do ?
what are you gonnas say ?
<br>
getting that feeling again, oh oh oh
mesmerizing thoughts
intuitions or believings
but no words to put it out
<br>
thinking 'till you understand things
getting it real clear in your head
talking about it, knowing what you think
but unable to make it clear, damn

<br>

what are you gonna do ?
what are you gonnas say ?
<sub><sup>keep it for yourself</sup></sub>
what are you gonna do ?
what are you gonnas say ?
<br>
getting that feeling again, oh oh oh
mesmerizing thoughts
intuitions or believings
but no words to put it out


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
**5 - put_it_out**
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})
