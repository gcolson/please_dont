---
title: titles.lisbeth
layout: page
permalink: /lyrics/lisbeth
---


lisbeth
=======
<br>
you come here every night
and when you don't, i cry
when you go out of sight
well, miss you i might

<br>

you're the cutest
yet so jealous
oh, lisbeth
you must be a chicken
with hypnotizing visions
oh, lisbeth
you're the cutest
yet so jealous
oh, lisbeth
you must be a chicken
with hypnotizing visions
oh, lisbeth

<br>

we never need to speak
'cause every talk fits in a blink
oh, curled up on my sit
on purring you don't stint
oh, lisbeth, lisbeth, lisbeth
lisbeth
lisbeth
oh, lisbeth
lisbeth


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
**10 - lisbeth**