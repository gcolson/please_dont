---
title: titles.walking_in_the_city
layout: page
permalink: /lyrics/walking_in_the_city
---

walking_in_the_city
===================

<br>
just walking in the city
not having fun
but not beeing bored
just walking in the city

<br>

you smile when you realize
this was the only day
when the sun shined all day

<br>

but just when you had this thought
the sun hides behind some clouds
<br>
a squirrel looking at you
sitting on this bench in
front of St Leonard Church
looking at this squirrel looking at you
looking at this squirrel looking at you
<br>
man, you feel great
just walking in the city
so stand up, yeah
stand up from this bench
keep on walking, go on

<br>

for it seems to be
the only thing
that can push away the clouds
from the sky and from your head


[wandering_around]({{ '/music' | relative_url }})
---------
**1 - walking_in_the_city**
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
3 - [in_and_out]({{ '/lyrics/in_and_out' | relative_url }})
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})
