---
title: titles.in_and_out
layout: page
permalink: /lyrics/in_and_out
---

in_and_out
=====
<br>
when i go out of here
for too long
it's like i forget
who i really am

<br>

when i stay in here
for too long
it feels like i lost track
of the world outside
<br>
<sub><sup>so, i have to keep going
in and out
'cause that's the only thing
that makes me feel good right now
yeah</sup></sub>
<br>
so i gotta keep going
in and out, in and out
'cause that's the only thing
that makes me feel good right now

<br>

yeah i, i have to keep going
in and out, in and out
'cause that's the only thing
that makes me feel good right now

<br>

so, i gotta keep going
in and out, in and out
'cause that's the only thing
that makes me feel good right now


[wandering_around]({{ '/music' | relative_url }})
---------
1 - [walking_in_the_city]({{ '/lyrics/walking_in_the_city' | relative_url }})
2 - [nothing_to_say]({{ '/lyrics/nothing_to_say' | relative_url }})
**3 - in_and_out**
4 - [wandering_around]({{ '/lyrics/wandering_around' | relative_url }})
5 - [put_it_out]({{ '/lyrics/put_it_out' | relative_url }})
6 - [please_don't]({{ "/lyrics/please_don't" | relative_url }})
7 - [round_down_round]({{ '/lyrics/round_down_round' | relative_url }})
8 - [441]({{ '/lyrics/441' | relative_url }})
9 - [through_the_years]({{ '/lyrics/through_the_years' | relative_url }})
10 - [lisbeth]({{ '/lyrics/lisbeth' | relative_url }})