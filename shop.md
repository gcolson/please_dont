---
layout: page
title: titles.shop
namespace: shop
permalink: /shop/
---

<b>{% t pages.shop.main_title %}</b>

{% t pages.shop.description %}

<div class="side_by_side">
    <img src="/assets/images/front_album.jpg" class="child" alt="photo du CD de face">
    <img src="/assets/images/back_album.jpg" class="child" alt="photo du CD de dos">
    <img src="/assets/images/inside_album.jpg" class="child" alt="photo de l'intérieur du CD">
</div>


<div id="smart-button-container">
  <div style="text-align: center;">
    <div id="paypal-button-container"></div>
  </div>
</div>


<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR" data-sdk-integration-source="button-factory"></script>
<script>
function initPayPalButton() {
  paypal.Buttons({
    style: {
      shape: 'rect',
      color: 'black',
      layout: 'vertical',
      label: 'buynow',
      
    },

    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{"description":"Tirage limité en version Compact Disc digipack de l'album \"wandering_around\" de please_don't. 50 exemplaires disponibles","amount":{"currency_code":"EUR","value":12,"breakdown":{"item_total":{"currency_code":"EUR","value":10},"shipping":{"currency_code":"EUR","value":2},"tax_total":{"currency_code":"EUR","value":0}}}}]
      });
    },

    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details) {
        alert('Transaction completed by ' + details.payer.name.given_name + '!');
      });
    },

    onError: function(err) {
      console.log(err);
    }
  }).render('#paypal-button-container');
}
initPayPalButton();
</script>